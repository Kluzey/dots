# Dots

my cool and epic dot files

## Software in Use

- Neovim
- Echinus
- St
- Polybar
- Htop
- Fish
- Cowsay
- Neofetch

### Screenshot

![Screenshot](screenshot.png "Screenshot of dots")
