set autoindent
filetype off

set termguicolors

set number

colorscheme pablo

syntax on

set nocursorcolumn
set nocursorline
set norelativenumber
syntax sync minlines=256

set nocompatible
set backspace=indent,eol,start
set ruler
set showcmd

set mouse=a
